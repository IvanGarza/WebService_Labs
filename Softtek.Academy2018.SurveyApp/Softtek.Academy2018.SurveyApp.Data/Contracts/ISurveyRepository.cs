﻿using Softtek.Academy2018.SurveyApp.Domain.Model;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Softtek.Academy2018.SurveyApp.Data.Contracts
{
    public interface ISurveyRepository : IGenericRepository<Survey>
    {

    }
}
